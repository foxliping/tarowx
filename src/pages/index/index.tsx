/*
 * @Author: liping.hu <liping.hu@onecontract.com>
 * @Date: 2022-04-08 10:00:15
 * @LastEditTime: 2022-04-08 10:26:38
 * @LastEditors: your name
 * @Description:
 */
import React, {useState, useEffect} from "react";
import { View, Text } from "@tarojs/components";

import './index.less'

const Index = () => {

  const [data, setData] = useState<number[]>([]);

  useEffect(()=>{
    const arr:number[] = [1,2,3];
    setData(arr);
  }, []);
  return (
    <View className='wrapper'>
      {data.map(item=>{
        return (
          <Text>{item}</Text>
        );
      })}
    </View>
  );
};

export default Index;
