/*
 * @Author: liping.hu <liping.hu@onecontract.com>
 * @Date: 2022-04-08 10:00:15
 * @LastEditTime: 2022-04-08 10:01:53
 * @LastEditors: your name
 * @Description:
 */
import { Component } from "react";
import "./app.less";

class App extends Component {
  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}

  componentDidCatchError() {}

  // this.props.children 是将要会渲染的页面
  render() {
    return this.props.children;
  }
}

export default App;
